﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Builder
{
   public class TableInfo
    {

        /// <summary>
        /// 全名，包括数据库
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// 表名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 表的字段 key字段名称，value 字段类型 （如int string long）
        /// </summary>
        public List<Field> Fields;

        /// <summary>
        /// 主键名称
        /// </summary>
        public string PrimaryKeyName { get; set; }

    }

    public class Field
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public Type FieldType { get; set; }
    }

}
