﻿namespace DAL_Builder
{
    partial class MainFormA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxDB = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxDBFilter = new System.Windows.Forms.TextBox();
            this.listBoxTable = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxTableFilter = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkedListBoxField = new System.Windows.Forms.CheckedListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.richTextBoxCopy = new System.Windows.Forms.RichTextBox();
            this.btnBuildEntity = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.radioBtnAnd = new System.Windows.Forms.RadioButton();
            this.radioBtnOr = new System.Windows.Forms.RadioButton();
            this.btnSelectPage = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxDB
            // 
            this.listBoxDB.FormattingEnabled = true;
            this.listBoxDB.ItemHeight = 12;
            this.listBoxDB.Location = new System.Drawing.Point(6, 47);
            this.listBoxDB.Name = "listBoxDB";
            this.listBoxDB.Size = new System.Drawing.Size(157, 376);
            this.listBoxDB.TabIndex = 0;
            this.listBoxDB.SelectedIndexChanged += new System.EventHandler(this.listBoxDB_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxDBFilter);
            this.groupBox1.Controls.Add(this.listBoxDB);
            this.groupBox1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(169, 430);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "数据库列表";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(6, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 11);
            this.label1.TabIndex = 3;
            this.label1.Text = "筛选";
            // 
            // textBoxDBFilter
            // 
            this.textBoxDBFilter.Location = new System.Drawing.Point(35, 20);
            this.textBoxDBFilter.Name = "textBoxDBFilter";
            this.textBoxDBFilter.Size = new System.Drawing.Size(128, 21);
            this.textBoxDBFilter.TabIndex = 2;
            this.textBoxDBFilter.TextChanged += new System.EventHandler(this.textBoxDBFilter_TextChanged);
            // 
            // listBoxTable
            // 
            this.listBoxTable.FormattingEnabled = true;
            this.listBoxTable.ItemHeight = 12;
            this.listBoxTable.Location = new System.Drawing.Point(6, 47);
            this.listBoxTable.Name = "listBoxTable";
            this.listBoxTable.Size = new System.Drawing.Size(188, 376);
            this.listBoxTable.TabIndex = 2;
            this.listBoxTable.SelectedIndexChanged += new System.EventHandler(this.listBoxTable_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBoxTableFilter);
            this.groupBox2.Controls.Add(this.listBoxTable);
            this.groupBox2.Location = new System.Drawing.Point(187, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 430);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "数据库表";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(6, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 11);
            this.label2.TabIndex = 4;
            this.label2.Text = "筛选";
            // 
            // textBoxTableFilter
            // 
            this.textBoxTableFilter.Location = new System.Drawing.Point(36, 20);
            this.textBoxTableFilter.Name = "textBoxTableFilter";
            this.textBoxTableFilter.Size = new System.Drawing.Size(158, 21);
            this.textBoxTableFilter.TabIndex = 3;
            this.textBoxTableFilter.TextChanged += new System.EventHandler(this.textBoxTableFilter_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnSelectPage);
            this.groupBox3.Controls.Add(this.radioBtnOr);
            this.groupBox3.Controls.Add(this.radioBtnAnd);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.btnSelect);
            this.groupBox3.Controls.Add(this.btnUpdate);
            this.groupBox3.Controls.Add(this.btnDelete);
            this.groupBox3.Controls.Add(this.btnInsert);
            this.groupBox3.Controls.Add(this.btnBuildEntity);
            this.groupBox3.Location = new System.Drawing.Point(393, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(210, 106);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "操作";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkedListBoxField);
            this.groupBox4.Location = new System.Drawing.Point(393, 124);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(210, 318);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "表字段";
            // 
            // checkedListBoxField
            // 
            this.checkedListBoxField.CheckOnClick = true;
            this.checkedListBoxField.FormattingEnabled = true;
            this.checkedListBoxField.Location = new System.Drawing.Point(6, 20);
            this.checkedListBoxField.Name = "checkedListBoxField";
            this.checkedListBoxField.Size = new System.Drawing.Size(188, 292);
            this.checkedListBoxField.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.richTextBoxCopy);
            this.groupBox5.Location = new System.Drawing.Point(609, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(455, 430);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "生成内容";
            // 
            // richTextBoxCopy
            // 
            this.richTextBoxCopy.Location = new System.Drawing.Point(6, 20);
            this.richTextBoxCopy.Name = "richTextBoxCopy";
            this.richTextBoxCopy.Size = new System.Drawing.Size(453, 404);
            this.richTextBoxCopy.TabIndex = 0;
            this.richTextBoxCopy.Text = "";
            // 
            // btnBuildEntity
            // 
            this.btnBuildEntity.Location = new System.Drawing.Point(7, 18);
            this.btnBuildEntity.Name = "btnBuildEntity";
            this.btnBuildEntity.Size = new System.Drawing.Size(75, 23);
            this.btnBuildEntity.TabIndex = 0;
            this.btnBuildEntity.Text = "生成类";
            this.btnBuildEntity.UseVisualStyleBackColor = true;
            this.btnBuildEntity.Click += new System.EventHandler(this.btnBuildEntity_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(7, 47);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(45, 23);
            this.btnInsert.TabIndex = 1;
            this.btnInsert.Text = "增";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(58, 47);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(45, 23);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "删";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(109, 47);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(45, 23);
            this.btnUpdate.TabIndex = 3;
            this.btnUpdate.Text = "改";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(160, 47);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(45, 23);
            this.btnSelect.TabIndex = 4;
            this.btnSelect.Text = "查";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "条件连接:";
            // 
            // radioBtnAnd
            // 
            this.radioBtnAnd.AutoSize = true;
            this.radioBtnAnd.Checked = true;
            this.radioBtnAnd.Location = new System.Drawing.Point(71, 80);
            this.radioBtnAnd.Name = "radioBtnAnd";
            this.radioBtnAnd.Size = new System.Drawing.Size(41, 16);
            this.radioBtnAnd.TabIndex = 6;
            this.radioBtnAnd.TabStop = true;
            this.radioBtnAnd.Text = "and";
            this.radioBtnAnd.UseVisualStyleBackColor = true;
            // 
            // radioBtnOr
            // 
            this.radioBtnOr.AutoSize = true;
            this.radioBtnOr.Location = new System.Drawing.Point(119, 80);
            this.radioBtnOr.Name = "radioBtnOr";
            this.radioBtnOr.Size = new System.Drawing.Size(35, 16);
            this.radioBtnOr.TabIndex = 7;
            this.radioBtnOr.Text = "or";
            this.radioBtnOr.UseVisualStyleBackColor = true;
            // 
            // btnSelectPage
            // 
            this.btnSelectPage.Location = new System.Drawing.Point(119, 18);
            this.btnSelectPage.Name = "btnSelectPage";
            this.btnSelectPage.Size = new System.Drawing.Size(86, 23);
            this.btnSelectPage.TabIndex = 8;
            this.btnSelectPage.Text = "分页查";
            this.btnSelectPage.UseVisualStyleBackColor = true;
            this.btnSelectPage.Click += new System.EventHandler(this.btnSelectPage_Click);
            // 
            // MainFormA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1076, 458);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "MainFormA";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainFormA";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxDB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxDBFilter;
        private System.Windows.Forms.ListBox listBoxTable;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxTableFilter;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckedListBox checkedListBoxField;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Button btnBuildEntity;
        private System.Windows.Forms.RichTextBox richTextBoxCopy;
        private System.Windows.Forms.RadioButton radioBtnOr;
        private System.Windows.Forms.RadioButton radioBtnAnd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSelectPage;
    }
}